# docker

[![Maintainer](https://img.shields.io/badge/maintained%20by-labotekh-e00000?style=flat-square)](https://gilab.com/labotekh)
[![License](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/labotekh/iac/roles/docker/-/blob/main/LICENSE)
[![Release](https://gitlab.com/labotekh/iac/roles/docker/-/badges/release.svg)](https://gitlab.com/labotekh/iac/roles/docker/-/releases)[![Ansible version](https://img.shields.io/badge/ansible-%3E%3D2.17-black.svg?style=flat-square&logo=ansible)](https://github.com/ansible/ansible)

Install Docker

**Platforms Supported**:

| Platform | Versions |
|----------|----------|
| Ubuntu | 24.04.1 |

## ⚠️ Requirements

Ansible >= 2.17.

## ⚡ Installation

### Install with git

If you do not want a global installation, clone it into your `roles_path`.

```bash
git clone https://gitlab.com/labotekh/iac/roles/docker.git  docker
```

But I often add it as a submodule in a given `playbook_dir` repository.

```bash
git submodule add https://gitlab.com/labotekh/iac/roles/docker.git roles/docker
```

### ✏️ Example Playbook

Basic usage is:

```yaml
- hosts: all
  roles:
    - role: docker
      vars:
        docker_fingerprint: 0EBFCD88
        docker_packages:
          - docker-ce
          - docker-ce-cli
          - containerd.io
          - docker-buildx-plugin
          - docker-compose-plugin
          - python3-docker
        docker_url: https://download.docker.com/linux/ubuntu
        docker_users:
          - xxxxxxxxxx
          - xxxxxxxxxx
        required_packages:
          - apt-transport-https
          - ca-certificates
          - acl
          - curl
          - software-properties-common
          - python3-pip
          - virtualenv
          - python3-setuptools
        
```

## ⚙️ Role Variables

Variables are divided in three types.

The **default vars** section shows you which variables you may
override in your ansible inventory. As a matter of fact, all variables should
be defined there for explicitness, ease of documentation as well as overall
role manageability.

The **context variables** are shown in section below hint you
on how runtime context may affects role execution.

### Default variables

#### main

Install Docker

| Variable Name | Required | Type | Default | Elements | Description |
|---------------|----------|------|---------|----------|-------------|
| docker_users | True | list |  | str | The users to add to docker group |
| docker_url | False | str | https://download.docker.com/linux/ubuntu |  | The url to download the docker package |
| docker_fingerprint | False | str | 0EBFCD88 |  | The docker package fingerprint |
| required_packages | False | list | ['apt-transport-https', 'ca-certificates', 'acl', 'curl', 'software-properties-common', 'python3-pip', 'virtualenv', 'python3-setuptools'] | str | The required packages to install Docker |
| docker_packages | False | list | ['docker-ce', 'docker-ce-cli', 'containerd.io', 'docker-buildx-plugin', 'docker-compose-plugin', 'python3-docker', 'docker-compose'] | str | The packages installed to have Docker |

## Author Information

Imotekh <imotekh@protonmail.com>